# Order Simulation

Aplikasi Order Simulation mempunyai 2 jenis user Login: User dan Driver. Aplikasi memiliki 1 User dan 2 Driver. User melakukan pesanan dan Driver mengambil pesanan untuk diantar ke alamat tujuan. Apabila pesanan telah diambil oleh salah satu Driver, 

# Business Process

# 1. User melakukan login dan memilih SKU untuk dipesan

<img src="images/1-create-order.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 2. User mengisi alamat tujuan pesanan

<img src="images/2-input-delivery-address.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 3. Pesanan telah terbuat dengan rincian status dan waktu terbuatnya

<img src="images/3-order-created.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 4. Driver melakukan login dan melihat ada pesanan yang perlu diproses

<img src="images/4-driver-view-order.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 5a. Driver mengambil pesanan tersebut untuk diproses

<img src="images/5-driver-job-created.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 5b. Driver lain yang login sudah tidak dapat melihat order yang telah diambil oleh driver pertama

<img src="images/5-driver-other-dont-see-order.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 6. Driver siap untuk berangkat dan mengubah status order menjadi "On Delivery"

<img src="images/6-driver-on-delivery.png" alt="alt text" title="Create Order" style="max-width:100%;">

# 7. Driver telah tiba di tujuan dan mengubah status order menjadi "Delivered"

<img src="images/7-driver-order-delivered.png" alt="alt text" title="Create Order" style="max-width:100%;">

# Data Diagram

<img src="images/0-data-diagram.png" alt="alt text" title="Create Order" style="max-width:100%;">